(function(angular){

    angular.module('news.module')
        .service('newsDataService', newsDataService);


    newsDataService.$inject = ['$http', '$q'];

    function newsDataService($http, $q){
        var self = this;

        self.fetch = fetchNews;

        return self;
        ///////////////////////////
        // Functions
        ///////////////////////////

        function fetchNews(){
            return $q(function(resolve, reject){
                $http.get('assets/news.json').then(function(result){
                    resolve(result.data);
                }).catch(reject);
            });
        }

    }
})(angular);