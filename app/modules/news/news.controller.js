(function(angular){


    angular.module('news.module')
        .controller('newsController', newsController);


    newsController.$inject = ['$scope', 'newsDataService'];

    /**
     * News page handler
     * @param $scope
     * @param newsDataService
     */
    function newsController($scope, newsDataService){
        $scope.init = initNewsController;



        /////////////////////////////////
        // Functions
        ////////////////////////////////

        function initNewsController(){
            newsDataService.fetch()
                .then(function(result){
                    $scope.news = result;
                })
                .catch(function(err){
                    console.error(err);
                })
        }
    }

})(angular);